package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Complete the repeatedString function below.
func repeatedString(s string, n int64) int64 {
	count := int64(0)
	size := int64(len(s))

	//find a's in string
	for i := int64(0); i < size; i++ {
		if s[i] == 'a' {
			count++
		}
	}

	//multiply by number of times string will fully duplicate
	count = count * (n / size)

	//find a's in final partial string
	for i := (n - n%size); i < n; i++ {
		if s[i%size] == 'a' {
			count++
		}
	}

	return int64(count)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	s := readLine(reader)

	n, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)

	result := repeatedString(s, n)

	fmt.Printf("%d\n", result)
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

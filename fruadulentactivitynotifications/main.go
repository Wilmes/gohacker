package main

import (
	"bufio"
	"container/list"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

// Complete the activityNotifications function below.
func activityNotifications(expenditure []int32, d int32) int32 {
	alerts := int32(0)
	size := int32(len(expenditure))

	//not enough days for alerts to happen
	if d >= size {
		return int32(0)
	}

	//gather initial buffer
	medList := list.New()
	sortedMedArr := make([]int32, d)
	for i := int32(0); i < d; i++ {
		medList.PushBack(expenditure[i])
		sortedMedArr[i] = expenditure[i]
	}

	//initial sort
	sort.Sort(int32Slice(sortedMedArr))

	//for all days past the buffer...
	for i := d; i < size; i++ {
		med := getMedian(sortedMedArr)
		// fmt.Println(sortedMedArr)
		// fmt.Println(med)
		// fmt.Println(expenditure[i])

		if float32(expenditure[i]) >= 2*med {
			alerts++
		}

		//remove oldest buffer day
		old := medList.Front()
		medList.Remove(old)
		for j := int32(0); j < d; j++ {
			if sortedMedArr[j] == old.Value {
				shiftSliceLeft(sortedMedArr[j:], -1)
				break
			}
		}

		//add current day to buffer
		medList.PushBack(expenditure[i])
		for j := int32(0); j < d; j++ {
			if expenditure[i] <= sortedMedArr[j] || sortedMedArr[j] == -1 {
				shiftSliceRight(sortedMedArr[j:], expenditure[i])
			}
		}
	}

	return alerts
}

func shiftSliceLeft(slice []int32, suffix int32) {
	size := len(slice)
	for i := 0; i < size; i++ {
		if i == size-1 {
			slice[i] = suffix
		} else {
			slice[i] = slice[i+1]
		}
	}
}

func shiftSliceRight(slice []int32, prefix int32) {
	size := len(slice)
	for i := size - 1; i >= 0; i-- {
		if i == 0 {
			slice[i] = prefix
		} else {
			slice[i] = slice[i-1]
		}
	}
}

type int32Slice []int32

func (p int32Slice) Len() int           { return len(p) }
func (p int32Slice) Less(i, j int) bool { return p[i] < p[j] }
func (p int32Slice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

func getMedian(sortedDays []int32) float32 {
	//odd
	if len(sortedDays)%2 == 1 {
		return float32(sortedDays[len(sortedDays)/2])
	}

	//even
	return (float32(sortedDays[len(sortedDays)/2-1]) + float32(sortedDays[len(sortedDays)/2])) / 2.0
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	nd := strings.Split(readLine(reader), " ")

	nTemp, err := strconv.ParseInt(nd[0], 10, 64)
	checkError(err)
	n := int32(nTemp)

	dTemp, err := strconv.ParseInt(nd[1], 10, 64)
	checkError(err)
	d := int32(dTemp)

	expenditureTemp := strings.Split(readLine(reader), " ")

	var expenditure []int32

	for i := 0; i < int(n); i++ {
		expenditureItemTemp, err := strconv.ParseInt(expenditureTemp[i], 10, 64)
		checkError(err)
		expenditureItem := int32(expenditureItemTemp)
		expenditure = append(expenditure, expenditureItem)
	}

	result := activityNotifications(expenditure, d)

	fmt.Printf("%d\n", result)
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

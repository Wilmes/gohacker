package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// Complete the minimumBribes function below.
func minimumBribes(q []int32) {
	numCuts := 0
	cutCount := make(map[int32]int)
	for i := 0; i < len(q); i++ {
		for j := i; j < len(q)-1; j++ {
			if q[j] > q[j+1] {
				//make sure number of cuts by q[j] is less than 2
				if cutCount[q[j]] >= 2 {
					fmt.Println("Too chaotic")
					return
				}

				//add cut to counters
				cutCount[q[j]] = cutCount[q[j]] + 1
				numCuts++

				//swap
				q[j], q[j+1] = q[j+1], q[j]

				fmt.Println(q)
			}
		}
	}
	fmt.Println(numCuts)
}

func cut(line []int32, i int) []int32 {
	line[i], line[i-1] = line[i-1], line[i]
	return line
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		n := int32(nTemp)

		qTemp := strings.Split(readLine(reader), " ")

		var q []int32

		for i := 0; i < int(n); i++ {
			qItemTemp, err := strconv.ParseInt(qTemp[i], 10, 64)
			checkError(err)
			qItem := int32(qItemTemp)
			q = append(q, qItem)
		}

		minimumBribes(q)
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

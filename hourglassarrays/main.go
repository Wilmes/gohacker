package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

// Complete the hourglassSum function below.
func hourglassSum(arr [][]int32) int32 {
	highestSum := int32(math.MinInt32)
	glasses := [16]cord{{0, 0}, {0, 1}, {0, 2}, {0, 3},
		{1, 0}, {1, 1}, {1, 2}, {1, 3},
		{2, 0}, {2, 1}, {2, 2}, {2, 3},
		{3, 0}, {3, 1}, {3, 2}, {3, 3}}

	for _, glass := range glasses {
		if sum := sumHourglass(arr, glass); sum > highestSum {
			highestSum = sum
		}
	}

	return int32(highestSum)
}

type cord struct {
	X int
	Y int
}

func sumHourglass(arr [][]int32, topLeftPoint cord) int32 {
	return arr[topLeftPoint.Y][topLeftPoint.X] + arr[topLeftPoint.Y][topLeftPoint.X+1] + arr[topLeftPoint.Y][topLeftPoint.X+2] +
		arr[topLeftPoint.Y+1][topLeftPoint.X+1] +
		arr[topLeftPoint.Y+2][topLeftPoint.X] + arr[topLeftPoint.Y+2][topLeftPoint.X+1] + arr[topLeftPoint.Y+2][topLeftPoint.X+2]
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	var arr [][]int32
	for i := 0; i < 6; i++ {
		arrRowTemp := strings.Split(readLine(reader), " ")

		var arrRow []int32
		for _, arrRowItem := range arrRowTemp {
			arrItemTemp, err := strconv.ParseInt(arrRowItem, 10, 64)
			checkError(err)
			arrItem := int32(arrItemTemp)
			arrRow = append(arrRow, arrItem)
		}

		if len(arrRow) != int(6) {
			panic("Bad input")
		}

		arr = append(arr, arrRow)
	}

	result := hourglassSum(arr)

	fmt.Printf("%d\n", result)
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

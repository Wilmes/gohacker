package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

// Complete the whatFlavors function below.
func whatFlavors(cost []int32, money int32) {
	//create flavor structs and sot by cost
	flavors := make([]flavor, len(cost))
	for i, c := range cost {
		flavors[i].Id = i + 1
		flavors[i].Cost = int(c)
	}
	sort.Sort(byCost(flavors))

	//start by adding the cheapest and most expensive
	i, j := len(flavors)-1, 0
	for i >= 0 && j < len(flavors) {
		if i == j { //don't want the same flavor twice
			i--
			continue
		}

		total := int32(flavors[i].Cost + flavors[j].Cost)
		if total == money { //found it
			ans := []int{flavors[i].Id, flavors[j].Id}
			sort.Ints(ans)
			fmt.Printf("%d %d\n", ans[0], ans[1])
			return
		} else if total > money { //move the the next most expensive
			i--
		} else { //move to the next cheapest
			j++
		}
	}
}

type flavor struct {
	Id   int
	Cost int
}

type byCost []flavor

func (a byCost) Len() int           { return len(a) }
func (a byCost) Less(i, j int) bool { return a[i].Cost < a[j].Cost }
func (a byCost) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	tTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	t := int32(tTemp)

	for tItr := 0; tItr < int(t); tItr++ {
		moneyTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		money := int32(moneyTemp)

		nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
		checkError(err)
		n := int32(nTemp)

		costTemp := strings.Split(readLine(reader), " ")

		var cost []int32

		for i := 0; i < int(n); i++ {
			costItemTemp, err := strconv.ParseInt(costTemp[i], 10, 64)
			checkError(err)
			costItem := int32(costItemTemp)
			cost = append(cost, costItem)
		}

		whatFlavors(cost, money)
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

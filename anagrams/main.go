package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

// Complete the makeAnagram function below.
func makeAnagram(a string, b string) int32 {
	alphabet := make([]int, 26)
	alphaStart := 97

	for _, r := range []rune(a) {
		alphabet[int(r)-alphaStart]++
	}

	for _, r := range []rune(b) {
		alphabet[int(r)-alphaStart]--
	}

	fmt.Println(alphabet)

	count := 0
	for _, letter := range alphabet {
		if letter > 0 {
			count += letter
		} else if letter < 0 {
			count -= letter
		}
	}

	return int32(count)
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	a := readLine(reader)

	b := readLine(reader)

	res := makeAnagram(a, b)

	fmt.Printf("%d\n", res)
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
